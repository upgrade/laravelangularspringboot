<?php


// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'ProdutoController@listar');

Route::get('/produtos', 'ProdutoController@listar');
Route::get('/produtos/mostra/{id}', 'ProdutoController@mostra');
Route::get('/produtos/remove/{id}', 'ProdutoController@remove');
Route::get('/produtos/novo', 'ProdutoController@novo');
Route::post('/produtos/adiciona', 'ProdutoController@adiciona');
Route::get('/produtos/retornajson', 'ProdutoController@retornaJson');

Route::get('/login', 'LoginController@form');
Route::post('/login', 'LoginController@login');


//  Auth::routes();

//  Route::get('/home', 'HomeController@index')->name('home');


