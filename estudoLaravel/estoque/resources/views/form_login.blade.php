@extends('layout.principal')
@section('conteudo')


    <form action="/login" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" class="form-control">
        </div>
       
        <div class="form-group">
            <label for="">Senha</label>
            <input type="password" name="password" class="form-control">
        </div>

        
        <button class="btn btn-primary" type="submit">Login</button>
    </form>

@stop