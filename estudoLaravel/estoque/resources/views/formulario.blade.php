@extends('layout.principal')
@section('conteudo')


<div class="alert alert-danger">
    <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
        @endforeach    
    </ul>
</div>

    <form action="/produtos/adiciona" method="post">

        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group">
            <label for="">Nome</label>
            <input type="text" name="nome" class="form-control">
        </div>
       
        <div class="form-group">
            <label for="">Valor</label>
            <input type="text" name="valor" class="form-control">
        </div>

       <div class="form-group">
            <label for="">Quantidade</label>
            <input type="text" name="quantidade" class="form-control">
        </div>

        <div class="form-group">
            <label for="">Tamanho</label>
            <input type="text" name="tamanho" class="form-control">
        </div>

        <div class="form-group">
            <label for="">Categoria</label>
            <select name="categoria_id" class="form-control">
                @foreach($categorias as $c)
                <option value="{{$c->id}}">{{$c->nome}}</option>
                @endforeach
            </select>
        </div>


        <div class="form-group">
            <label for="">Descrição</label>
            <textarea name="descricao" id="" cols="30" rows="2" class="form-control"></textarea>
        </div>
        <button class="btn btn-primary" type="submit">Adicionar</button>
    </form>

@stop