<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/css/app.css" >
    <link href="/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <title>Controle de Estoque</title>
</head>
<body>
    <div class="container">

        <nav class="navbar navbar-default">
        <div class="container-fluid">

        <div class="navbar-header">      
            <a class="navbar-brand" href="/produtos">Estoque Laravel</a>
        </div>

            <ul class="nav navbar-nav navbar-right">
            <li><a href="/produtos">Listagem</a></li>
            @if (Auth::guest())
                <li><a href="/auth/login">Login</a></li>
                <li><a href="/auth/register">Register</a></li>
            @else
                <li>{{ Auth::user()->name }} </li>
                <li><a href="/auth/logout">Logout</a></li>
            @endif
            </ul>

        </div>
        </nav>

        
        @yield('conteudo')

        <footer class="footer">
            <p>© Curso de Laravel do Alura.</p>
        </footer>
  </div>

</body>
</html>