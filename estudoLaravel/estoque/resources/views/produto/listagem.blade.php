@extends('layout.principal')
@section('conteudo')

@if(empty($produtos))
<div class="alert alert-danger">
    Você não tem nehum produto cadastrado
</div>

@else

<h1>Listagem de Produtos</h1>
<table class="table">
    @foreach($produtos as $p)
    <tr class="{{ $p->quantidade <= 1 ? 'table-danger' : ''}}" >
        <td>{{ $p->nome }}</td>
        <td>{{ $p->valor }}</td>
        <td>{{ $p->descricao }}</td>
        <td>{{ $p->quantidade }}</td>
        <td>{{ $p->tamanho }}</td>
        <td>{{ $p->categoria->nome }}</td>
        <td>
            <a href="/produtos/mostra/{{$p->id }}"><i class="fas fa-search"></i></a>
        </td>
        <td>
            <a href="/produtos/remove/{{$p->id }}"><i class="fas fa-trash"></i></a>
        </td>
    </tr>
    @endforeach    
</table>

@if(old('nome'))
    <div class="alert alert-success">
        Produto {{old('nome')}} adicionado com sucesso
    </div>
@endif

@endif
@stop
