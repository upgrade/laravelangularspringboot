<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use Validator;
use App\Produto;
use App\Http\Requests\ProdutoRequest;
use Auth;
use App\Categoria;

class ProdutoController extends Controller{

    public function __construct()
    {
        $this->middleware('autorizador');
    }


    public function listar(){

        $produtos = Produto::all();
        return view('produto.listagem')->with('produtos', $produtos);
    }

    public function mostra($id){
        $produto = Produto::find($id);
        return view('produto.detalhes')->with('p', $produto);
    }

    public function novo(){
        return view('formulario')->with('categorias', Categoria::all());
    }

    public function adiciona(ProdutoRequest $request){

        Produto::create($request->all());
        return redirect()->action('ProdutoController@listar')->withInput(Request::only('nome'));
    }

    public function remove($id){
        $produto = Produto::find($id);
        $produto->delete();
        return redirect()->action('ProdutoController@listar');
    }

    public function retornaJson(){
        $produtos = DB::select('select * from produtos');
        return response()->json($produtos);
    }

}