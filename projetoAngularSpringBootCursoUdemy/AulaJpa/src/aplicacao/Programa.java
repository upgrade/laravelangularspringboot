package aplicacao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import dominio.Pessoa;

public class Programa {

	public static void main(String[] args) {
		//instanciando
//		Pessoa p1 = new Pessoa(null, "Carlos silva", "carlos@gmail.com");
//		Pessoa p2 = new Pessoa(null, "Ana clara", "ana@gmail.com");
//		Pessoa p3 = new Pessoa(null, "Silvia", "silvia@gmail.com");
		
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("exemplo-jpa");
		EntityManager em = emf.createEntityManager();
		
		//inserindo no db
//		em.getTransaction().begin();
//		em.persist(p1);
//		em.persist(p2);
//		em.persist(p3);
//		em.getTransaction().commit();
		
		//buscando pessoa
//		Pessoa p = em.find(Pessoa.class, 2);
//		System.out.println(p);
//		System.out.println("Pronto");
		
		//buscando e removendo
//		Pessoa p = em.find(Pessoa.class, 2);
//		em.getTransaction().begin();
//		em.remove(p);
//		em.getTransaction().commit();
		
		em.close();
		emf.close();
		
	}

}
