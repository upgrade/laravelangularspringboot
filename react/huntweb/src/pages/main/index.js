import React, {Component} from 'react';
import api from '../../services/api'
import {Link} from 'react-router-dom';

import './styles.css';

export default class Main extends Component {

    state = {
        //estado da variavel para que assim ela carrega e não apenas instanciar ela
        products:[],
        productInfo:{},
        page: 1
    }


    //executa logo q o component é exibido em tela
    componentDidMount(){
        this.loadProducts();
    }
    loadProducts = async (page = 1) => {
        const response = await api.get(`/products?page=${page}`);

        const {docs, ...productInfo} = response.data;

        //aqui pega pela requisição o conteudo do products nesse caso
       this.setState({products: docs, productInfo, page})
        
    }

    prevPage = () =>{
        const {page, productInfo} = this.state;

        if(page ===1) return;

        const pageNumber = page - 1;

        this.loadProducts(pageNumber);
    }

    nextPage = () =>{
        const {page, productInfo} = this.state;
        if(page === productInfo.page) return;

        const pageNumber = page + 1;
        this.loadProducts(pageNumber);
    }

   render(){
    //variavel products interno e não a do estado acima
    const {products, page, productInfo} = this.state;

       //pega o que foi armazenado e verifica a quantidade nesse caso
       //return <h1>Contagem de Produtos: {this.state.products.length}</h1>

       return (
           <div className="product-list">
                {products.map(product => (
                    <article key={product._id}>
                        <strong>{product.title}</strong>
                        <p>{product.description}</p>
                        <Link to={`/products/${product._id}`}>Acessar</Link>
                    </article>
                ))}

                <div className="actions">
                    <button disabled={page ===1} onClick={this.prevPage}>Anterior</button>
                    <button disabled={page == productInfo.pages} onClick={this.nextPage}>Proximo</button>
                </div>

           </div>
       )
   }
}