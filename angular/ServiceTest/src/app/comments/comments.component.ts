import { CommentModelo } from './comment.modelo';
import { CommentService } from './comment.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comments: CommentModelo[];

  constructor(private commentsService: CommentService) { }

  ngOnInit() {
    this.commentsService.getComments()
      .subscribe(comment => this.comments = comment);
  }

}
