const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
mongoose.Promise = Promise;
const requireDir = require('require-dir')

// Iniciando o App
const app = express();

//permitir enviar dados da aplicação em formato json
app.use(express.json());
app.use(cors());

// Iniciando o DB
mongoose.connect('mongodb://localhost:27017/nodeapi', { useNewUrlParser: true });

requireDir('./src/models');

//Rotas
app.use('/api', require("./src/routes"));


app.listen(3001)

