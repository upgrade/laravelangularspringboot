--Lembrete que instalei tudo no linux mint 18

npm init -y - criar o arquivo json com o comportamento da aplicação

npm install express - dependencia que ajuda nas rotas e views

package-lock.json - cache para saber as dependencias ja instaladas e assim não precisa sempre reinstalar as dependencias

npm install -D nodemon - para recarregar a requisição toda vez q alterar o arquivo

devDependences - apenas dependencias para desenvolvimento

Instalar mongoDb pelo docker, antes instalar o docker

----instalação do docker mint 18
Primeiro remova versões antigas chamadas docker ou docker-engine :  

sudo apt-get remove docker docker-engine docker.io
Atualize o indice de pacotes com o  apt:

sudo apt-get update
Instalar pacotes para permitir que o apt use um repositório via HTTPS:

sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
Adicionar chave GPG oficial do Docker:

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - 
Use o seguinte comando para configurar o repositório estável:

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable" 
Observação: Com o comando  lsb_release -cs você pode consultar o nome da sua distribuição do Ubuntu, no caso a distribuição Linux Mint 18  ela é baseada no Ubuntu 16.04 Xenial.

Atualize novamente a lista de pacotes:

sudo apt-get update
Finalmente instale a última versão do Docker CE:

sudo apt-get install docker-ce

--fim instalação

sudo docker run --name mongoDB -p 27017:27017 -d mongo -> criar e rodar o mongoDB

https://robomongo.org/download -> baixar o roboet para gerenciar o banco do mongo

sudo docker ps -a -> ver os bancos criados

sudo docker start mongoDB - para rodar o mongo

npm install mongoose -> ORM de banco de dados com o mongoDB apenas usando codigos JS

Caso houver problemas com  a porta use o comando -> lsof -i tcp:numeroPorta para verificar
quem esta usando depois o comando kill -9 PID

Se o problema persistir primeiro start no mongo e depois rode o npm run dev

Instalar insomnia para testar as rotas -> https://support.insomnia.rest/article/23-installation#ubuntu

instalar modulo para paginação -> npm install mongoose-paginate

instalar o cors para as requisições diferentes -> npm install cors



