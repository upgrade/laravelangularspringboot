// A partir do seguinte vetor:
// var nomes = ["Diego", "Gabriel", "Lucas"];
// Preencha uma lista (<ul>) no HTML com os itens da seguinte forma:
// l Diego
// l Gabriel
// l Lucas

var nomes = ["Diego", "Gabriel", "Lucas"];
var listaUl = document.querySelector('.container .listaCompleta ul');
var btnEelement = document.querySelector('.container #botao');
var inputNome = document.querySelector('.container #nome');
function preencherLista(){

    for(let no of nomes){
        var list =  document.createElement('li');
        var texto = document.createTextNode(no);
        list.appendChild(texto);
        listaUl.append(list);
    }
    
  

}
btnEelement.onclick = function(){
    var list =  document.createElement('li');
    var texto = document.createTextNode(inputNome.value);
    list.appendChild(texto);
    listaUl.append(list);
    inputNome.value = "";
}
preencherLista();


// 4º exercício
// Seguindo o resultado do exercício anterior adicione um input em tela e um botão como a seguir:
// <input type="text" name="nome">
// <button onClick="adicionar()">Adicionar</button>
// Ao clicar no botão, a função adicionar() deve ser disparada adicionando um novo item a lista de
// nomes baseado no nome preenchido no input e renderizando o novo item em tela juntos aos
// demais itens anteriores. Além disso, o conteúdo do input deve ser apagado após o clique.