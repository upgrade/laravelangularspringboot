// Crie um botão que ao ser clicado cria um novo elemento em tela com a forma de um quadrado
// vermelho com 100px de altura e largura. Sempre que o botão for clicado um novo quadrado deve
// aparecer na tela.

var caixas = document.querySelector('.container .caixas');
var btn = document.querySelector('.container #botao');


btn.onclick = function(){

    var teste = document.createElement('div');
    teste.className='caixaGerada';
    teste.style.width = '100px';
    teste.style.height = '100px';
    teste.style.margin = '10px';
    teste.style.backgroundColor = getRandomColor();

    teste.onmouseover = function(){
        teste.style.backgroundColor = getRandomColor();
    };

    var cx =  document.querySelector('.container .caixas');
    
    
    cx.appendChild(teste);


}


// 2º exercício
// Utilizando o resultado do primeiro desafio, toda vez que o usuário passar o mouse por cima de
// algum quadrado troque sua cor para uma cor aleatória gerada pela função abaixo:
function getRandomColor() {
    var letters = "0123456789ABCDEF";
    var color = "#";
    for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
    }
var newColor = getRandomColor();