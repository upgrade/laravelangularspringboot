//Escreva uma função que dado um total de anos de estudo retorna o quão experiente o usuário é:

var anos = document.querySelector('.container .anos').textContent;
var exp = document.querySelector('.container .experiencia');

function experiencia(anos){

    if(anos >= 0 && anos < 1){
        var texto = document.createTextNode("O usuario tem " + anos + " anos de experiencia então é Iniciante");
        exp.appendChild(texto);
    }

    if(anos >= 1 && anos < 3){
        var texto = document.createTextNode("O usuario tem " + anos + " anos de experiencia então é Intermediário");
        exp.appendChild(texto);
    }

    if(anos >= 3 && anos < 6){
        var texto = document.createTextNode("O usuario tem " + anos + " anos de experiencia então é Avançado");
        exp.appendChild(texto);
    }

    if(anos >= 7){
        var texto = document.createTextNode("O usuario tem " + anos + " anos de experiencia então é Jedi Master");
        exp.appendChild(texto);
    }

    //alert(anos)

}
experiencia(anos);


//var anosEstudo = 7;

// De 0-1 ano: Iniciante
// De 1-3 anos: Intermediário
// De 3-6 anos: Avançado
// De 7 acima: Jedi Master