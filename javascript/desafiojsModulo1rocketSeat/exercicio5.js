//Dado o seguinte vetor de objetos:
var usuarios = [
    {
        nome: "Diego",
        habilidades: ["Javascript", "ReactJS", "Redux"]
    },
    {
        nome: "Gabriel",
        habilidades: ["VueJS", "Ruby on Rails", "Elixir"]
    }
];

var usuariosHtml = document.querySelector('.container .usuarios');


function usuario(usuarios){

    for(let us of usuarios){
        
        if(us.nome == "Diego"){
            var texto = document.createTextNode("O " + us.nome + " Possui as habilidades: " + us.habilidades.join(',') );
           
            usuariosHtml.appendChild(texto);
           
        }
       
        if(us.nome == "Gabriel"){
            var texto = document.createTextNode("O " + us.nome + " Possui as habilidades: " + us.habilidades.join(',') );
            usuariosHtml.appendChild(texto);
        }
        
    }
}
usuario(usuarios);


// Escreva uma função que produza o seguinte resultado:
// O Diego possui as habilidades: Javascript, ReactJS, Redux
// O Gabriel possui as habilidades: VueJS, Ruby on Rails, Elixir