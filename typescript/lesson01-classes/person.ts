export class Person {

    private name: string;

    constructor(name: string){
        this.name = name;
    }

    public showAge(age: number): void{
        console.log(`${this.name} has ${age} yeras old`);
        
    }

    toString(): string{
        return `Class person, name ${this.name}`;
    }

}